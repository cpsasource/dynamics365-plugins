﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Linq;

namespace CRMD.CPSA.NotePlugin.Helper
{
    public class CountryLookupHelper
    {
        public static Entity RetrieveByName(IOrganizationService service, string name, bool? active = true)
        {
            if (!string.IsNullOrEmpty(name))
            {
                return service.RetrieveMultiple(new QueryExpression
                {
                    EntityName = "crmd_country",
                    ColumnSet = new ColumnSet(true),
                    Criteria =
                {
                    Conditions =
                    {
                        new ConditionExpression("statecode", ConditionOperator.Equal, active.GetValueOrDefault(false) ? 0 : 1),
                        new ConditionExpression("crmd_name", ConditionOperator.Equal, name)
                    }
                }

                }).Entities.FirstOrDefault();
            }

            return null;
        }
    }
}
